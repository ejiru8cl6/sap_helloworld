sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"newtype/work/HelloWorld/model/models"
], function(UIComponent, Device, models) {
	"use strict";

	return UIComponent.extend("newtype.work.HelloWorld.Component", {

		metadata: {
			manifest: "json",
			rootView: "newtype.work.HelloWorld.view.App",
			routing: {
				config: {
					routerClass: "sap.m.routing.Router",
					viewPath: "newtype.work.HelloWorld.view",
					controlId: "rootControl",
					controlAggregation: "pages",
					viewType: "XML"
				},
				routes: [{
					name: "MainPage",
					// empty hash - normally the start page
					pattern: "",
					target: "MainPage"
				}, {
					name: "HelloWorld",
					pattern: "HelloWorld",
					target: "HelloWorld"
				}, {
					name: "Layout",
					pattern: "Layout",
					target: "Layout"
				}],
				targets: {
					MainPage: {
						viewName: "MainPage",
						viewLevel: 0
					},
					HelloWorld: {
						viewName: "HelloWorld",
						viewLevel: 1
					},
					Layout: {
						viewName: "Layout",
						viewLevel: 1
					}
				}
			}

		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function() {
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);

			// set the device model
			//this.setModel(models.createDeviceModel(), "device");
			this.getRouter().initialize();
		}
	});
},true);