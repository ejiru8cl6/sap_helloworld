sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast",
	"sap/ui/model/resource/ResourceModel",
	 "sap/ui/core/routing/History"
], function(Controller,MessageToast,ResourceModel,History) {
	"use strict";
	
	return Controller.extend("newtype.work.HelloWorld.controller.HelloWorld", {
		//Init
		onInit : function() {
			var oDropdownBox1 = this.byId('Lang');   
			var oItem = new sap.ui.core.ListItem("en-US");
			oItem.setText("en-US");
			oDropdownBox1.addItem(oItem);
			oItem = new sap.ui.core.ListItem("zh-CN");
			oItem.setText("zh-CN");
			oDropdownBox1.addItem(oItem);
			oItem = new sap.ui.core.ListItem("zh-TW");
			oItem.setText("zh-TW");
			oDropdownBox1.addItem(oItem);
			
			var default_Lang="en-US";
			sap.ui.getCore().getConfiguration().setLanguage(default_Lang);
			oDropdownBox1.setSelectedItem(default_Lang);
			
		//	var sUrl = "#" + this.getOwnerComponent().getRouter().getURL("page2");
	//		this.byId("link").setHref(sUrl);
		},
		//BT EVENT
		HelloBT_Event : function(){
        var oResource = new ResourceModel({
        	bundleName: "newtype.work.HelloWorld.i18n.HelloWorld"
        });
       	MessageToast.show(oResource.getProperty("hellowBT"));
       },
       //ChangeLang
       ChangeLang : function(){
       	var oDropdownBox1 = this.byId('Lang');  
       	var Lang=oDropdownBox1.getSelectedItemId();
		sap.ui.getCore().getConfiguration().setLanguage(Lang);
		//var sLocale = sap.ui.getCore().getConfiguration().getLanguage();
       },
       ToMainPage : function () {
       		var sPreviousHash = History.getInstance().getPreviousHash();

    		if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				this.getOwnerComponent().getRouter().navTo("MainPage", null, true);
			}
		}
	});
});

// Component.js, manifest.json, neo-app.json 用途

/*
Component.js :  Component.js 文件是組件控制器，並提供運行時元數據和組件方法。

manifest.json : manifest.json 文件定義了有關應用程序的靜態信息，例如應用程序的名稱或各種文件的位置，被稱為 Application Descriptor，
它是用JavaScript對象表示法（JSON）格式編寫的。

neo-app.json : neo-app.json 文件包含SAP Web IDE的所有項目設置，並在項目的根文件夾中創建。這是一個由多個配置鍵組成的JSON格式的文件。配置中最重要的設置是啟動應用程序時SAPUI5運行時所在的路徑。
*/