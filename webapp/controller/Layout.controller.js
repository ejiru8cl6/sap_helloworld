sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast",
	"sap/ui/model/resource/ResourceModel",
	'sap/m/MessageBox',
	'sap/m/Popover',
	'sap/m/Button',
	"sap/ui/core/routing/History"
], function(Controller, MessageToast, ResourceModel, MessageBox, Popover, Button, History) {
	"use strict";

	return Controller.extend("newtype.work.HelloWorld.controller.Layout", {
		//Init
		onInit: function() {

		},
		Event_MessageBox: function(oEvent) {
			//ㄏ還不懂傳參數,自訂標籤  先拿TEXT 判斷
			var Type = oEvent.getSource().getText();

			switch (Type) {
				case "Confirm":
					//confirm
					MessageBox.confirm(
						"confirm"
					);
					break;
				case "Alert":
					//alert
					MessageBox.alert(
						"alert"
					);
					break;
				case "Error":
					//error
					MessageBox.error(
						"error"
					);
					break;
				case "Info":
					//information
					MessageBox.information(
						"information"
					);
					break;
				case "warning":
					//warning
					MessageBox.warning(
						"warning", {
							title: "warning",
							actions: [MessageBox.Action.YES, MessageBox.Action.NO, "Other Button"]
						}
					);
					break;
				case "Success":
					//success
					MessageBox.success(
						"success"
					);
					break;
				case "MoreAction_Error":
					MessageBox.error(
						"MoreAction_Error", {
							actions: ["Manage Products", sap.m.MessageBox.Action.CLOSE],
							onClose: function(sAction) {
								MessageToast.show("Action selected: " + sAction);
							}
						}
					);
					break;
				default:
					MessageToast.show("No Event!");
					break;
			}

		},
		Event_BT: function(oEvent) {
			MessageToast.show(oEvent.getSource().getText() + " Click!");
		},
		ToMainPage: function() {
			var sPreviousHash = History.getInstance().getPreviousHash();

			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				this.getOwnerComponent().getRouter().navTo("MainPage", null, true);
			}
		}
	});
});

/*
	<Button text="Confirm"  press="Message_Confirm" />
								<Button text="Alert"  press="Message_Alert" />
								<Button text="Error"  press="Message_Error" />
								<Button text="Info"  press="Message_Info" />
								<Button text="Waming"  press="Message_Waming" />
								<Button text="Success"  press="Message_Success" />
								<Button text="MoreAcrion_Error"  press="Message_MA_Error" />
								<Button text="MoreAcrion_Waming"  press="Message_MA_Waming" />
								*/