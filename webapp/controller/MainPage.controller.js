sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast",
	"sap/ui/model/resource/ResourceModel",
	'sap/ui/model/json/JSONModel'
], function(Controller, MessageToast, ResourceModel,JSONModel) {
	"use strict";

	return Controller.extend("newtype.work.HelloWorld.controller.MainPage", {
		//Init
		onInit: function() {

		},
		ToHelloWorld: function() {
			this.getOwnerComponent().getRouter().navTo("HelloWorld");
		},
		ToLayout: function() {
			this.getOwnerComponent().getRouter().navTo("Layout");
		}
	});
});